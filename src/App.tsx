import React, {useState, useEffect} from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import Home from "./components/Home"
import NavigationBar from "./components/NavigationBar"

import Students from "./components/Students"
import Tasks from "./components/Tasks"
import Marks from "./components/Marks"

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <BrowserRouter>
        <NavigationBar />
        <div className="container">
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/students' element={<Students />} />
                <Route path='/tasks' element={<Tasks />} />
                <Route path='/marks' element={<Marks />} />
            </Routes>
        </div>
    </BrowserRouter>
  );
}

export default App;
