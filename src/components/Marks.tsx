import React, { useState, useEffect } from 'react'
import { variables } from './Settings'
import { Table, FormControl } from 'react-bootstrap'

interface Task {
    id: number

    name: string,
    maxMark: number,
    description: string,
    priority: string,
    created: Date,
    startAccept: Date,
    closeAccept: Date,
}

interface StudentTask {
    taskId: number,
    studentId: number,
    mark: number,
    finished: Date,
}

interface Student {
    studentId: number
    firstName: string
    lastName: string
    marks: StudentTask[]
}

const Marks: React.FC = () => {
    const [students, setStudents] = useState<Student[]>([]);
    const [tasks, setTasks] = useState<Task[]>([]);
    useEffect(() => {
        refreshModels()
    }, [])

    const refreshModels = () => {
        fetch(variables.API_URL + "/" + variables.STUDENTS_TASK)
            .then((response) => response.json())
            .then((data) => setStudents([...data]))

        fetch(variables.API_URL + "/" + variables.TASKS)
            .then((response) => response.json())
            .then((data) => setTasks([...data]))
    }

    const updateST = (m: StudentTask) => {
        fetch(variables.API_URL + "/" + variables.MARKS, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(m)
        })
            .then((res) => res.json())
            .catch((error) => console.log(error))
    }

    return (
        <div>
            <Table striped bordered>
                <thead>
                    <tr>
                        <td>Firstname</td>
                        <td>Lastname</td>
                        {
                            tasks.map((t) => {
                                return <td>{t.name}</td>
                            })
                        }
                        <td>Total</td>
                    </tr>
                </thead>
                <tbody>
                    {students.map((s: Student) => {
                        return (
                            <tr>
                                <td>{s.firstName}</td>
                                <td>{s.lastName}</td>
                                {tasks.map((t) => {
                                    let studentTask: StudentTask | undefined = s.marks.find((s) => s.taskId === t.id)
                                    if (studentTask === undefined) {
                                        studentTask = {
                                            studentId: s.studentId,
                                            taskId: t.id,
                                            mark: 0,
                                            finished: new Date()
                                        }
                                    }
                                    return <td>
                                        <FormControl type="number"
                                            defaultValue={studentTask.mark}
                                            onChange={(e) => {
                                                studentTask!.finished = new Date()
                                                studentTask!.mark = parseFloat(e.target.value)
                                                updateST(studentTask!)
                                                refreshModels()
                                            }}
                                        ></FormControl>
                                    </td>
                                })}
                                <td>{s.marks.reduce((pr, curMark) => pr + curMark.mark, 0)}</td>
                            </tr>)
                    })}
                </tbody>
            </Table>
        </div>
    )
}

export default Marks