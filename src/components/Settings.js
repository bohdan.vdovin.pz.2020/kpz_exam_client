export const variables = {
    API_URL: "http://localhost:5243/api",
    MODELS: "model",
    STUDENTS: "Student",
    MARKS: "StudentTask",
    TASKS: "Task",
    STUDENTS_TASK: "StudentTask/all"
}
