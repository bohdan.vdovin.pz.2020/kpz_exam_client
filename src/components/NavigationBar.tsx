import React from 'react'
import {GiTeacher} from 'react-icons/gi'

const NavigationBar: React.FC = () => {
    return (
        <nav className='navbar navbar-dark navbar-expand-md bg-dark'>
            <a className='navbar-brand ms-3' href='/'>
                <GiTeacher size={40} />
            </a>
            <div className='collapse navbar-collapse' id='navbarNav'>
                <ul className='navbar-nav mn-auto'>
                    <li className='nav-item'>
                        <a className='nav-link' href='/students'>
                            Students
                        </a>
                    </li>
                    <li className='nav-item'>
                        <a
                            className='nav-link'
                            href='/marks'
                        >
                            Marks
                        </a>
                    </li>
                    <li className='nav-item'>
                        <a  className='nav-link' href='/tasks'>
                            Tasks
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}
export default NavigationBar
