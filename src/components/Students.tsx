import React, { useState, useRef, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { variables } from './Settings'
import {
    FormGroup,
    Form,
    Modal,
    Button,
    FormLabel,
    FormControl,
    Container,
    Row,
    Table
} from 'react-bootstrap'
import { MdDeleteOutline, MdModeEditOutline } from 'react-icons/md'

interface Student {
    id: number
    firstName: string
    lastName: string
    age: number
    email: string
    phone: string
}

const Home: React.FC = () => {
    const [students, setStudents] = useState<Student[]>([])
    const [show, setShow] = useState(false)
    const [curStudent, setCurStudent] = useState<Student>({
        id: -1,
        firstName: "",
        lastName: "",
        email: "",
        age: 0,
        phone: ""
    })

    useEffect(()=>{
        refreshModels();
    }
    ,[])
    const formRef = useRef<HTMLFormElement>(null)

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const refreshModels = () => {
        fetch(variables.API_URL + "/" + variables.STUDENTS)
            .then((response) => response.json())
            .then((data) => setStudents([...data]))
    }
    const createModel = (m: Student) => {
        m.id = 0
        fetch(variables.API_URL + "/" + variables.STUDENTS, {
            method: 'POST',
            headers : {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(m)
        })
            .then((res) => res.json())
            .then((data) => refreshModels())
            .catch((error) => console.log(error))
    }
    const updateModel = (m: Student) => {
        fetch(variables.API_URL + "/" + variables.STUDENTS, {
            method: 'PUT',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(m)
        })
            .then((res) => res.json())
            .then((data) => refreshModels())
            .catch((error) => console.log(error))
    }
    const deleteModel = (m: Student) => {
        // Fetch to delete
        fetch(variables.API_URL + "/" + variables.STUDENTS + "/" + m.id.toString(), {
            method: 'DELETE',
        })
            .then((res) => res.json())
            .then((data) => refreshModels())
            .catch((error) => console.log(error))
    }

    const createClick = () => {
        const new_model: Student = {
            id: -1,
            firstName : "",
            lastName : "",
            age: 0,
            phone: "",
            email: ""
        }
        setCurStudent(new_model)
        handleShow()
    }
    const editClick = (m: Student) => {
        setCurStudent(m)
        handleShow()
    }
    const deleteClick = (m: Student) => {
        deleteModel(m)
        refreshModels()
    }

    return (
        <div>
            <Table striped bordered hover >
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>FirstName</td>
                        <td>LastName</td>
                        <td>Age</td>
                        <td>Email</td>
                        <td>Phone</td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    {students.map((model: Student) => {
                        return (
                            <tr>
                                <td>{model.id}</td>
                                <td>{model.firstName}</td>
                                <td>{model.lastName}</td>
                                <td>{model.age}</td>
                                <td>{model.email}</td>
                                <td>{model.phone}</td>
                                <td>
                                    <Button
                                        variant='secondary'
                                        onClick={() => deleteModel(model)}
                                    >
                                        <MdDeleteOutline />
                                    </Button>
                                </td>
                                <td>
                                    <Button
                                        variant='secondary'
                                        onClick={() => editClick(model)}
                                    >
                                        <MdModeEditOutline />
                                    </Button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
            {/* <Container> */}
            {/* <Row className="justify-content-center"> */}
            <Button className='col' variant='dark' onClick={createClick}>
                Add new Student <br/>
            </Button>
            {/* </Row> */}
            {/* </Container> */}
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h3>Create new model</h3>
                    <Form ref={formRef}>
                        <Form.Group controlId='name'>
                            <Form.Label>Firstname</Form.Label>
                            <Form.Control
                                name='firstname'
                                type='text'
                                defaultValue={curStudent.firstName}
                                onChange={(e) =>
                                    setCurStudent({
                                        ...curStudent,
                                        firstName: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Lastname</Form.Label>
                            <Form.Control
                                name='lastname'
                                defaultValue={curStudent.lastName}
                                type='text'
                                onChange={(e) =>
                                    setCurStudent({
                                        ...curStudent,
                                        lastName: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                                name='age'
                                type='number'
                                defaultValue={curStudent.age}
                                onChange={(e) =>
                                    setCurStudent({
                                        ...curStudent,
                                        age: parseInt(e.target.value)
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Phone</Form.Label>
                            <Form.Control
                                name='phone'
                                defaultValue={curStudent.phone}
                                type='text'
                                onChange={(e) =>
                                    setCurStudent({
                                        ...curStudent,
                                        phone: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                defaultValue={curStudent.email}
                                name='email'
                                type='text'
                                onChange={(e) =>
                                    setCurStudent({
                                        ...curStudent,
                                        email: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='secondary' onClick={handleClose}>
                        Close
                    </Button>
                    {curStudent?.id === -1 ? (
                        <Button
                            variant='primary'
                            type='submit'
                            onClick={(e) => {
                                e.preventDefault()
                                createModel(curStudent)
                                handleClose()
                            }}
                        >
                            Create
                        </Button>
                    ) : (
                        <Button
                            variant='primary'
                            type='submit'
                            onClick={(e) => {
                                e.preventDefault()
                                updateModel(curStudent)
                                handleClose()
                            }}
                        >
                            Edit
                        </Button>
                    )}
                </Modal.Footer>
            </Modal>{' '}
        </div>
    )
}

export default Home
