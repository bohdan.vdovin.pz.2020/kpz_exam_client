import React, { useState, useRef, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { variables } from './Settings'
import {
    FormGroup,
    Form,
    Modal,
    Button,
    FormLabel,
    FormControl,
    Container,
    Row,
    Table
} from 'react-bootstrap'
import { MdDeleteOutline, MdModeEditOutline } from 'react-icons/md'
import { BiDownload } from 'react-icons/bi'
import { AiOutlinePlusSquare } from 'react-icons/ai'

function mockFile(): File {
    return new File(['foo'], 'foo.txt', { type: 'text/plain' })
}

interface Task {
    id: number

    name: string,
    maxMark: number,
    description: string,
    priority: string,
    created: Date,
    startAccept: Date,
    closeAccept: Date,
}

const Home: React.FC = () => {
    const [tasks, setTasks] = useState<Task[]>([])
    const [show, setShow] = useState(false)
    const [curTask, setCurTask] = useState<Task>({
        id: -1,
        name: "",
        maxMark: -1,
        description: "",
        priority: "",
        created: new Date(),
        closeAccept: new Date(),
        startAccept: new Date()
    })
    const formRef = useRef<HTMLFormElement>(null)
    useEffect(()=>{
        refreshModels()
    },[])

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const refreshModels = () => {
        fetch(variables.API_URL + "/" + variables.TASKS)
            .then((response) => response.json())
            .then((data) => setTasks(data))
    }

    const createModel = (m: Task) => {
        m.created = new Date()
        fetch(variables.API_URL + "/" + variables.TASKS, {
            method: 'POST',
            headers : {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(m)
        })
            .then((res) => res.json())
            .then((data) => refreshModels())
            .catch((error) => console.log(error))
    }
    const updateModel = (m: Task) => {
        fetch(variables.API_URL + "/" + variables.TASKS, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(m)
        })
            .then((res)=>res.json())
            .then((data)=> refreshModels())
            .catch((error) => console.log(error))
    }
    const deleteModel = (m: Task) => {
        fetch(variables.API_URL + "/" + variables.TASKS + "/" + m.id.toString(), {
            method: 'DELETE',
        })
            .then((res) => res.json())
            .then((data) => refreshModels())
            .catch((error) => console.log(error))
    }

    const createClick = () => {
        const new_model: Task = {
            id: -1,
            name: "",
            maxMark: -1,
            description: "",
            priority: "",
            created: new Date(),
            closeAccept: new Date(),
            startAccept: new Date()
        }
        setCurTask(new_model)
        handleShow()
    }
    const editClick = (m: Task) => {
        setCurTask(m)
        handleShow()
    }
    const deleteClick = (m: Task) => {
        deleteModel(m)
        refreshModels()
    }

    return (
        <div>
            <Table striped bordered hover >
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Max mark</td>
                        <td>Descripiton</td>
                        <td>Priority</td>
                        <td>Created</td>
                        <td>Accept</td>
                        <td>Finish</td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    {tasks.map((model: Task) => {
                        return (
                            <tr>
                                <td>{model.id}</td>
                                <td>{model.name}</td>
                                <td>{model.maxMark}</td>
                                <td>{model.description}</td>
                                <td>{model.priority}</td>
                                <td>{model.created.toString()}</td>
                                <td>{model.startAccept.toString()}</td>
                                <td>{model.closeAccept.toString()}</td>
                                <td>
                                    <Button
                                        variant='secondary'
                                        onClick={() => deleteModel(model)}
                                    >
                                        <MdDeleteOutline />
                                    </Button>
                                </td>
                                <td>
                                    <Button
                                        variant='secondary'
                                        onClick={() => editClick(model)}
                                    >
                                        <MdModeEditOutline />
                                    </Button>
                                </td>
                                <td></td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>

            <Button className='col' variant='dark' onClick={createClick}>
                Create Task <br />
            </Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Task</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form ref={formRef}>
                        <Form.Group controlId='name'>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                name='name'
                                type='text'
                                defaultValue={curTask.name}
                                onChange={(e) =>
                                    setCurTask({
                                        ...curTask,
                                        name: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Priority</Form.Label>
                            <Form.Control
                                name='priority'
                                type='text'
                                defaultValue={curTask.priority}
                                onChange={(e) =>
                                    setCurTask({
                                        ...curTask,
                                        priority: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Max mark</Form.Label>
                            <Form.Control
                                name='maxMark'
                                type='number'
                                defaultValue={curTask.maxMark}
                                onChange={(e) =>
                                    setCurTask({
                                        ...curTask,
                                        maxMark: parseInt(e.target.value)
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                name='description'
                                defaultValue={curTask.description}
                                type='text'
                                onChange={(e) =>
                                    setCurTask({
                                        ...curTask,
                                        description: e.target.value
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Start Acceptance</Form.Label>
                            <Form.Control
                                name='startAccept'
                                defaultValue={curTask.startAccept.toString()}
                                type='text'
                                onChange={(e) =>
                                    setCurTask({
                                        ...curTask,
                                        startAccept: new Date(e.target.value)
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Close Acceptance</Form.Label>
                            <Form.Control
                                name='closeAccept'
                                defaultValue={curTask.closeAccept.toString()}
                                type='text'
                                onChange={(e) =>
                                    setCurTask({
                                        ...curTask,
                                        closeAccept: new Date(e.target.value)
                                    })
                                }
                            ></Form.Control>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='secondary' onClick={handleClose}>
                        Close
                    </Button>
                    {curTask?.id === -1 ? (
                        <Button
                            variant='primary'
                            type='submit'
                            onClick={(e) => {
                                e.preventDefault()
                                createModel(curTask)
                                handleClose()
                            }}
                        >
                            Create
                        </Button>
                    ) : (
                        <Button
                            variant='primary'
                            type='submit'
                            onClick={(e) => {
                                e.preventDefault()
                                updateModel(curTask)
                                handleClose()
                            }}
                        >
                            Edit
                        </Button>
                    )}
                </Modal.Footer>
            </Modal>{' '}
        </div>
    )
}

export default Home
